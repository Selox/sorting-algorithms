#include <stdio.h>
#include <stdlib.h>
#include "Sorting.h"
int i, j, k;
int min, max;
/****************************************************
 * Uses bubble sort to sort A[0..N-1]
 ****************************************************/
void BubbleSort(int A[], int N){
	for (i = 0; i < N - 1; i++)
	{
		for ( j = 0; j < N-1; j++)
		{
			if (A[j] > A[j + 1])
				swap(A[j], A[j + 1]);
		}
	}
} //end-BubbleSort

/****************************************************
 * Uses selection sort to sort A[0..N-1]
 ****************************************************/
void SelectionSort(int A[], int N){
		for (i = 0; i < N - 1; i++)
		{
			min = i;
			for (j = i + 1; j < N; j++)
				if (A[j] < A[min])
					min = j;
			swap(A[min], A[i]);
		}
} //end-SelectionSort

/****************************************************
 * Uses insertion sort to sort A[0..N-1]
 ****************************************************/
void InsertionSort(int A[], int N) {
	min = A[0];
	int flag;
	for (i = 0; i < N; i++) {
		flag = A[i];
		j = i - 1;
		while (j >= 0 && A[j] > flag)
		{
			A[j + 1] = A[j];
			j = j - 1;
		}
		A[j + 1] = flag;
	}
} //end-InsertionSort

/****************************************************
 * Uses mergesort to sort A[0..N-1]
 ****************************************************/
void MergeSort(int A[], int N){
	ParcalaSort(A, 0, N - 1);
} //end-MergeSort

/****************************************************
 * Uses quicksort sort to sort A[0..N-1]
 ****************************************************/
void QuickSort(int A[], int N){
	if (N <= 1) return;
	if (N == 2) 
	{
		if (A[0] > A[1]) swap(A[0], A[1]);
		return;
	}
	int index = Partition(A, N);
	QuickSort(A, index);
	QuickSort(A+index+1, N-index-1);
} //end-QuickSort

/****************************************************
 * Uses heapsort to sort A[0..N]
 * NOTICE: The first element is in A[0] not in A[1]
 ****************************************************/
void HeapSort(int A[], int N){
	for (int i = N / 2 - 1; i >= 0; i--)
		heaple(A, N, i);

	for (int i = N - 1; i > 0; i--)
	{
		swap(A[0], A[i]);
		heaple(A, i, 0);
	}
} //end-HeapSort

/****************************************************
 * Uses radixsort to sort A[0..N]
 ****************************************************/
void RadixSort(int A[], int N) {
	int max = A[0];
	for (int i = 1; i < N; i++)
		if (A[i] > max)
			max = A[i];

	for (int exp = 1; max / exp > 0; exp *= 10)
		Say�c�S�ralama(A, N, exp);
} //end-RadixSort

/****************************************************
 * Uses countingsort to sort A[0..N]
 ****************************************************/
void CountingSort(int A[], int N)
{
	Say�c�S�ralama(A, N, 1);
}

void swap(int &a, int &b) {
	int tmp = a;
	a = b;
	b = tmp;
}

void heaple(int A[], int n, int i)
{
	int max = i;
	int l = 2 * i + 1;
	int r = 2 * i + 2;

	if (l < n && A[l] > A[max])
		max = l;

	if (r < n && A[r] > A[max])
		max = r;

	if (max != i)
	{
		swap(A[i], A[max]);
		heaple(A, n, max);
	}
}

int Partition(int A[], int N) 
{
	int* B = new int[N];

	int j = 0;
	int ortanca = A[0];
	for (int i = 1; i < N; i++) 
	{
		if (A[i] < ortanca) B[j++] = A[i];
	} 

	int index = j;  
	B[j++] = ortanca;

	for (int i = 1; i < N; i++) 
	{
		if (A[i] >= ortanca) B[j++] = A[i];
	}

	for (int i = 0; i < N; i++) A[i] = B[i];
	delete[] B;
	return index;
}

void merge(int A[], int l, int m, int r)
{
	int x = m - l + 1;
	int y = r - m;
	int * Temp_L;
	int * Temp_R;
	Temp_L = new int[x];
	Temp_R = new int[y];

	for (i = 0; i < x; i++)
		Temp_L[i] = A[l + i];
	for (j = 0; j < y; j++)
		Temp_R[j] = A[m + 1 + j];

	i = 0; j = 0; k = l;
	while (i < x && j < y)
	{
		if (Temp_L[i] <= Temp_R[j])
		{
			A[k] = Temp_L[i];
			i++;
		}
		else
		{
			A[k] = Temp_R[j];
			j++;
		}
		k++;
	}
	while (i < x)
	{
		A[k] = Temp_L[i];
		i++;
		k++;
	}

	while (j < y)
	{
		A[k] = Temp_R[j];
		j++;
		k++;
	}
}

void ParcalaSort(int A[], int l, int r)
{
	if (l < r)
	{
		int m = l + (r - l) / 2;

		ParcalaSort(A, l, m);
		ParcalaSort(A, m + 1, r);
		merge(A, l, m, r);
	}

}

void SayiciSiralama(int A[], int N, int exp)
{
	int * output;
	output = new int[N];
	int i, count[10] = { 0 };

	for (i = 0; i < N; i++)
		count[(A[i] / exp) % 10]++;

	for (i = 1; i < 10; i++)
		count[i] += count[i - 1];

	for (i = N - 1; i >= 0; i--)
	{
		output[count[(A[i] / exp) % 10] - 1] = A[i];
		count[(A[i] / exp) % 10]--;
	}

	for (i = 0; i < N; i++)
		A[i] = output[i];
	
}